### To run Project2:

Install Dependencies:
-   install NodeJS
-   validate install by running: ```npm -v```
-   Run Command: ```npm install```

To Run Project:
- Run Project: ```node server.js```
    - Outputs the the hits/misses.


This code does what it is asked.
I would like to make the 'assumption' you have this running on linux, and would cut out this middle-man of the python/nodeJS engines:
```find {your_directory_to_Search} -type f -name '*whatever_your_searching_for' | wc -l```

I have went ahead and created that script, and provided the comparisons.



To Run the Shell Script: ```time sh findMatch.sh test '*py' ```
- outputs the time and output
- included a photo demonstrating this, roughly 20x faster.
