/* eslint-disable no-path-concat */

'use strict';

const findMatches = require('./findMatch');

// Get's matches for this folder and with this extension
findMatches.getMatches(__dirname + '/sampleFiles', '.py');
