#!/usr/bin/env bash

# Get variables from first and second params
# this could even be done in the find statement.
searchDirectory=$1
searchString=$2

# assign to array, and search hits
found=(`find $searchDirectory -type f -name $searchString | wc -l`)

# Find total number of files.
total=(`find $searchDirectory -type f | wc -l`)

# calculate misses, and return results
echo HIT: $found  Miss: $(expr $total - $found)
