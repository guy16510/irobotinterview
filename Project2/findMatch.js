/* eslint-disable no-magic-numbers,no-shadow,require-await */

'use strict';

const {
  promisify
} = require('util');
const {
  resolve
} = require('path');
const fs = require('fs');
const readdir = promisify(fs.readdir);
const stat = promisify(fs.stat);

/**
 * Given a list of files, recurse down each directory
 * @param {String} dir - this is the directory you are searching.
 * @returns {callback} getFilesRecursively, which is a promise to get all files.
 */
async function getFilesRecursively (dir) {
  // Read all subdirectories
  const subDirectories = await readdir(dir);
  const files = await Promise.all(subDirectories.map(async (subDirectories) => {
    const res = resolve(dir, subDirectories);

    return (await stat(res)).isDirectory() ? getFilesRecursively(res) : res;
  }));


  return files.reduce((a, f) => {
    return a.concat(f);
  }, []);
}

/**
 * Given a list of files, process each one and determine if it met your search criteria
 * @param {Array} fileArray - Provides a listing of all the files.
 * @param {String} search - file extension you are looking for
 * @returns {Object} tallyObj - which has hit or miss of search.
 */
async function processFiles (fileArray, search) {
  const tally = fileArray.filter(x => {
    return x.toLowerCase().indexOf(search) > -1;
  }).length;


  return {
    Hit: tally,
    Miss: fileArray.length - tally
  };
}

/**
 * Main runner of file. Series of async requests.
 * @param {String} filePath - Provides the reference of the filepath you are calling.
 * @param {String} search - file extension you are looking for
 * @returns {null} null
 */
async function getMatches (filePath, search) {
  getFilesRecursively(filePath)
    .then(files => {
      return processFiles(files, search);
    })
    .then(tallyObj => {
      return console.log(tallyObj.Hit + ' Hit and ' + tallyObj.Miss + ' Miss');
    })
    .catch(e => {
      return console.error(e);
    });
}

module.exports = {
  getFilesRecursively,
  getMatches,
  processFiles
};
