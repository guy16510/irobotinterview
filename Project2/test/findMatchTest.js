/* eslint-disable no-magic-numbers,no-path-concat */

'use strict';

const chai = require('chai');
const expect = chai.expect;
const findMatch = require('../findMatch');
const testFileArr = ['filetest.py', 'foo.bar', 'thisIsPython.py'];


describe('Checks the Find Match functions', () => {
  it('Validates function call works, and counts files', async () => {
    const result = await findMatch.getFilesRecursively(__dirname + '/../sampleFiles');

    expect(result.length).to.equal(6);
  });
  it('Validates the fileMatching is working', async () => {
    const result = await findMatch.processFiles(testFileArr, '.py');

    expect(result.Hit).to.equal(2);
    expect(result.Miss).to.equal(1);
  });
});
