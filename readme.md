# iRobot Inteview
### To run the code samples:

Install Dependencies:
-   install NodeJS
-   validate install by running: ```npm -v```
-   Run Command: ```npm install```

To Run Project:
- Run Project 1: ```npm run project1```
- Run Project 2: ```npm run project2```

To Test Project:
Testing for both projects
- Test Project 1: ```npm test-project1```
- Test Project 2: ```npm test-project2```

You can also go individually into each project, and manually run each project.
