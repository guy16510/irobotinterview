/* eslint-disable no-magic-numbers */

'use strict';

const chai = require('chai');
const expect = chai.expect;
const TextParser = require('../TextParser');
const textParserTest = '{id,operator,collectedWhen{id,dateTag,collectedWhen{id}, uploadedWhen},data}';
let lineParser;

before(() => {
  lineParser = new TextParser(textParserTest);
});

describe('Checks for the unSorted array letter conversions', () => {
  it('should be an array', () => {
    expect(lineParser.unsortedData).to.be.an('array');
  });
  it('should have a length of 9.', () => {
    expect(lineParser.unsortedData.length).to.equal(9);
  });
  it('unsorted data should have the correct markup.', () => {
    expect(lineParser.unsortedData).to.deep.equal([
      'id',
      'operator',
      'collectedWhen',
      '- id',
      '- dateTag',
      '- collectedWhen',
      '-- id',
      '- uploadedWhen',
      'data'
    ]);
  });
});

describe('Checks for the sorted array letter conversions', () => {
  it('should be an array', () => {
    expect(lineParser.sortedData).to.be.an('array');
  });
  it('should have a length of 9.', () => {
    expect(lineParser.sortedData.length).to.equal(9);
  });
  it('unsorted data should have the correct markup.', () => {
    expect(lineParser.sortedData).to.deep.equal([
      'collectedWhen',
      '- collectedWhen',
      'data',
      '- dateTag',
      'id',
      '- id',
      '-- id',
      'operator',
      '- uploadedWhen'
    ]);
  });
});
