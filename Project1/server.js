/* eslint-disable no-path-concat,no-sync,guard-for-in,no-unused-vars */

'use strict';

const fs = require('fs');
const TextParser = require('./TextParser');
// Splits the file into an array
const array = fs.readFileSync(__dirname + '/inputTestCases.txt').toString()
  .split('\n');

// Could move into TextParser object, however to make re-useable, put this here.
for (const i in array) {
  const lineParser = new TextParser(array[i]);
}
