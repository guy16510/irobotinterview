/* eslint-disable no-sync,no-negated-condition,brace-style,no-lonely-if,no-magic-numbers,guard-for-in,no-unused-vars,no-path-concat */

'use strict';

/* First pass. Just to conceptualize, somewhat working. ~20-30min. I left it in so you could see how I initially went about it.
    let tempObj = '{id,operator,collectedWhen{id,dateTag,collectedWhen{id}, uploadedWhen},data}';
    let tempObj = '{}';
    let dashes = 0;
    let finalArr = [];
    //a better way would be to loop through the nested objects? I think its all syntactically correct.
    for(let i in array) {
        //console.log(array[i]);
        console.log("New Element: " + array[i].toString());

        let tempInnerArray = array[i].split("{");
        let innerArray = tempInnerArray.filter((v) => (!!(v)==true));

        for(let k in innerArray){
            let tempItem =  innerArray[k].split(',');
            let item = tempItem.filter((v) => (!!(v)==true));

            for(let l in item){
                let finalOutPut = ('-').repeat(dashes) + " " + item[l].split('}')[0];
                console.log(finalOutPut);
                finalArr.push(finalOutPut);

                if(item[l].indexOf('}') > -1){
                    //if the length is 0, then its the end.
                    if(dashes !== 0)
                        dashes -= 1;
                }

            }
            dashes += 1;
        }

        //abc's
        console.log(finalArr.sort()); //TODO figure this out.
        finalArr = [];

        //set dashes back to 0
        dashes = 0;
    }
*/


// Spent roughly 2-3hrs, and thought about the object, then wrote up some code.
/**
 * Given a class called TextParser, create a constructor to hold while object does its magic.
 * @param {String} dir - this is the directory you are searching.
 * @returns {Object} all object properties.
 */
class TextParser {
  // RawData is the initial raw value of the data.
// eslint-disable-next-line require-jsdoc
  constructor (rawData) {
    // Main holder of the data
    this.unsortedData = [];
    // Main holder of the abc data
    this.sortedData = [];
    this.pointer = 0;
    this.arrayPointer = 0;
    this.rawData = rawData;
    this.dashCounter = 0;
    // Splits it character by character.
    this.recurseLines(this.pointer, this.rawData.split(''));
  }

  // Recurses each letter until the pointer === the array length.
  /**
     * Given an array of characters, this function recurses each letter until it completes
     * @param {String} pointer - this is the current array element its processing.
     * @param {String} currentLine - the char array holder of the sentence
     * @returns {Array} unSortedData is created, then calls the sortedData function
     */
  recurseLines (pointer, currentLine) {
    // Recurse and validate there is more data to process.
    if (pointer !== currentLine.length) {
      if (currentLine[pointer] === '{') {
        this.dashCounter += 1;
        this.arrayPointer += 1;
      } else if (currentLine[pointer] === ',') {
        // Add new line
        this.arrayPointer += 1;
      } else if (currentLine[pointer] === '}') {
        // Remove dash Unless it is at 0
        if (this.dashCounter !== 0)
          this.dashCounter -= 1;
      } else if (currentLine[pointer] === ' ' || currentLine[pointer] === '\r') {
        // Maybe log out if production needed to know random bad characters to clean?
        // Data should be cleaned before recurse on line: 71
      }

      // No special characters, add letters to word
      else {
        // Add letters to current array
        if (this.unsortedData[this.arrayPointer - 1]) {
          // Append to current string
          this.unsortedData[this.arrayPointer - 1] += currentLine[pointer];
        }
        // Initialize array since its not empty
        else {
          const dash = this.dashCounter === 1 ? '' : '-'.repeat(this.dashCounter - 1) + ' ';

          this.unsortedData[this.arrayPointer - 1] = dash + currentLine[pointer];
        }
      }
      this.pointer += 1;
      // TODO for space arr splice currentLine each time, and make currentLine get smaller. Keeping it incase you need to do other data transformations.
      this.recurseLines(this.pointer, currentLine);
    } else {
      // Done recursing
      this.sortedData = [...this.unsortedData];
      this.printUnsortedData();
      this.alphabetical(this.sortedData);
    }
  }

  /**
     * To Print out all data
     * @returns {null} Console.log Output...
     */
  printUnsortedData () {
    console.log('############# Data Parsed ###############');
    console.log(this.unsortedData.join('\n'));
  }

  // Makes assumptions that Capital letters come first.
  // Makes assumptions that dashes are ignored, and goes to abc's only
  /**
     * Given an unsorted array. This takes the array and sorts it without looking at the dashes.
     * @param {Array} arr - takes each string, and removes special char(s) '-' '--'. Then makes comparisons
     * @returns {Array} sortedData is created from this function.
     */
  alphabetical (arr) {
    arr.sort(
      (a, b) => {
        // Remove special characters.
        const first = a.replace(/[^A-Z0-9]+/ig, '');
        // For comparison
        const second = b.replace(/[^A-Z0-9]+/ig, '');

        if (first < second) {
          return -1;
        }
        if (first > second) {
          return 1;
        }

        return 0;
      }
    );
    this.sortedData = arr;
    this.printSortedData();
  }

  /**
     * To Print out all sorted data
     * @returns {null} Console.log Output...
     */
  printSortedData () {
    console.log('\n############# Data Alphabetized ###########');
    console.log(this.sortedData.join('\n'));
  }
}

module.exports= TextParser;

